#include <stdio.h>
#include <stdlib.h>
#include <pwd.h>
#include "cJSON.h"

int main()
{
	struct passwd  *pwd;
	char str[8];
	char *out;
	cJSON *root, *user, *users;

	root = cJSON_CreateObject();
	users = cJSON_CreateArray();

	cJSON_AddItemToObject(root, "users", users);

	setpwent();

	while((pwd = getpwent()) != NULL)
	{
		cJSON_AddItemToArray(users, user = cJSON_CreateObject());
		sprintf(str, "%d", pwd->pw_uid);
		cJSON_AddItemToObject(user, "user_id", cJSON_CreateString(str));
		cJSON_AddItemToObject(user, "username", cJSON_CreateString(pwd->pw_name));
		cJSON_AddItemToObject(user, "password", cJSON_CreateString(pwd->pw_passwd));
		sprintf(str, "%d", pwd->pw_gid);
		cJSON_AddItemToObject(user, "group_id", cJSON_CreateString(str));
		cJSON_AddItemToObject(user, "real_name", cJSON_CreateString(pwd->pw_gecos));
		cJSON_AddItemToObject(user, "directory", cJSON_CreateString(pwd->pw_dir));
		cJSON_AddItemToObject(user, "shell", cJSON_CreateString(pwd->pw_shell));
	}
	endpwent();

	out = cJSON_Print(root);
	printf("%s\n", out);
	free(out);

	cJSON_Delete(root);

	return 0;
}
