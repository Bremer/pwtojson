# pwtojson

This program reads the passwd file on Linux and Unix systems and converts it into a json data structure.

Run `make` to build project. An executable file called `pwtojson` is created. To redirect standard output to a file run `pwtojson >> passwd.json`
